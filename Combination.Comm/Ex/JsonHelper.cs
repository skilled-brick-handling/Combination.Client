﻿namespace Combination.Comm
{
    public static class JsonHelper
    {
        public static void ToJsonFile<T>(string path, T data)
        {
            var json = data.ToJsonString();
            File.WriteAllText(path, json);
        }

        public static T FromJsonFile<T>(string path)
        {
            var json = File.ReadAllText(path);
            return json.ToJsonObject<T>();
        }
    }
}