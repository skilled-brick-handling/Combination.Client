using System.Text.Json;

namespace Combination.Comm
{
    /// <summary>
    /// Object扩展方法
    /// </summary>
    public static class ObjectEx
    {
        /// <summary>
        /// 对象 to json字符串
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToJsonString(this object obj)
        {
            return JsonSerializer.Serialize(obj);
        }

        /// <summary>
        /// 源对象属性值复制到目标对象中的相同属性
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        public static void PropertyCopyTo(this object source, object target)
        {
            var sProps = source.GetType().GetProperties();
            var tProps = target.GetType().GetProperties();
            foreach (var sProp in sProps)
            {
                var tProp = tProps.FirstOrDefault(p => p.Name == sProp.Name);
                if (tProp == null || tProp.SetMethod == null)
                    continue;

                tProp.SetValue(target, sProp.GetValue(source));
            }
        }

    }
}
