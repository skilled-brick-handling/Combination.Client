using System.Collections.Generic;

namespace Combination.Data
{
    /// <summary>
    /// 
    /// </summary>
    public class WhereBuilderResult
    {
        public string Where { get; }
        public Dictionary<string, object> Param { get; }

        public WhereBuilderResult(string where, Dictionary<string, object> param)
        {
            this.Where = where;
            this.Param = param;
        }
    }
}
