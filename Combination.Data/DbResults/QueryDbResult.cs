using System.Collections.Generic;

namespace Combination.Data
{
    /// <summary>
    /// 查询数据结果
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class QueryDbResult<T> : DbResult<List<T>>
    {

    }
}
