namespace Combination.Data
{
    /// <summary>
    /// 查询单条数据结果
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class QueryFirstDbResult<T> : DbResult<T>
    {

    }
}
