namespace Combination.Data
{
    /// <summary>
    /// sqlserver
    /// </summary>
    public class SqlServerDb : Db
    {
        /// <summary>
        /// 数据库类型
        /// </summary>
        public override DbProvider DbProvider => DbProvider.SqlServer;
    }

    /// <summary>
    /// sqlserver
    /// </summary>
    public class MySqlDb : Db
    {
        /// <summary>
        /// 数据库类型
        /// </summary>
        public override DbProvider DbProvider => DbProvider.MySql;
    }
}
