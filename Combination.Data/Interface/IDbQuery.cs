using System;
using System.Linq.Expressions;

namespace Combination.Data
{
    public interface IDbQuery<T>
    {
        void Clear();
        SqlBuilderResult Build();
        IDbQuery<T> Where(Expression<Func<T, bool>> where);
        IDbQuery<T> And(Expression<Func<T, bool>> where);
        IDbQuery<T> Or(Expression<Func<T, bool>> where);
        IDbQuery<T> OrderBy<Field>(Expression<Func<T, Field>> fieldSelector, OrderByType type);
        PageDbResult<T> Page(int page, int size, int? timeout = null);
        ScalarDbResult<int> Count(int? timeout = null);
        QueryDbResult<T> ToList(int? timeout = null);
        QueryFirstDbResult<T> FirstOrDefault(int? timeout = null);
        QueryDbResult<T> Top(int top, int? timeout = null);
    }
}
