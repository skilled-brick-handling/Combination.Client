using System.Collections.Generic;

namespace Combination.Data
{
    public interface IDbInsert<T>
    {
        void Clear();
        SqlBuilderResult Build();
        IDbInsert<T> Values(IEnumerable<T> models);
        IDbInsert<T> Values(T model);
        ExecuteDbResult Execute(int? timeout = null);
    }
}
