using System;
using System.Linq.Expressions;

namespace Combination.Data
{
    public interface IDbDelete<T>
    {
        void Clear();
        SqlBuilderResult Build();
        IDbDelete<T> Where(Expression<Func<T, bool>> where);
        IDbDelete<T> And(Expression<Func<T, bool>> where);
        IDbDelete<T> Or(Expression<Func<T, bool>> where);
        ExecuteDbResult Execute(int? timeout = null);
    }
}
