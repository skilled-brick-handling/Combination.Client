namespace Combination.Data.Entity
{
    public class PageInfo
    {
        public int Page { get; }
        public int Size { get; }

        public PageInfo(int page, int size)
        {
            this.Page = page;
            this.Size = size;
        }
    }
}
