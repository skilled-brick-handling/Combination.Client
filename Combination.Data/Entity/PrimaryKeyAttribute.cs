using System;

namespace Combination.Data.Entity
{
    /// <summary>
    /// 主键标识
    /// </summary>
    [AttributeUsageAttribute(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class PrimaryKeyAttribute : Attribute
    {
    }
}
