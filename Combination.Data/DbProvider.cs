namespace Combination.Data
{
    public enum DbProvider
    {
        SqlServer = 0,
        MySql = 1,
        Sqlite = 2
    }
}
