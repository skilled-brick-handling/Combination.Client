﻿using System.Windows.Input;
using System.Windows;
using System.Windows.Controls;
using System.Reflection.Metadata;
using System.Linq;

namespace Combination.UI
{
    public static class CommonCommands
    {
        static CommonCommands()
        {
            CloseWindowCommand = new UICommand<Window>(CloseWindow);
            MinimizeWindowCommand = new UICommand<Window>(MinimizeWindow);
            MaximizeOrNormalWindowCommand = new UICommand<Window>(MaximizeOrNormalWindow);
            ButtonClickCommand = new UICommand(ButtonClick);
        }

        public static ICommand CloseWindowCommand { get; }
        public static ICommand MinimizeWindowCommand { get; }
        public static ICommand MaximizeOrNormalWindowCommand { get; }

        public static ICommand ButtonClickCommand { get; }

        public static void CloseWindow(Window window)
        {
            window.Close();
        }

        public static void MinimizeWindow(Window window)
        {
            window.WindowState = WindowState.Minimized;
        }

        public static void MaximizeOrNormalWindow(Window window)
        {
            if (window.WindowState == WindowState.Normal)
            {
                window.WindowState = WindowState.Maximized;
            }
            else if (window.WindowState == WindowState.Maximized)
            {
                window.WindowState = WindowState.Normal;
            }
            else if (window.WindowState == WindowState.Minimized)
            {
                window.WindowState = WindowState.Normal;
            }
        }
        public static void ButtonClick(object parameter)
        {
            var sources = (object[])parameter;
            if (sources.Length != 2)
                return;
            var window = (Window)sources[0];
            var messageButtons = (MessageButtons)sources[1];
            var curPages = window.GetValue(PagesProperty.ValueProperty);
            if (messageButtons == MessageButtons.Ok && curPages is FormPage form)
            {
                var ret = form.ValidateDataContext();
                if (!ret.CheckAccess)
                {
                    MessageBox.Show(ret.Message);
                    return;
                }
            }
            window.Close();
        }
    }
}
