﻿using System.Windows;

namespace Combination.UI
{
    public abstract class PageVm : UIVm
    {
        #region 生命周期触发方法
        internal void OnSourceLoaded(DependencyObject source)
        {
            OnLoaded(source);
        }
        internal void OnSourceUnloaded(DependencyObject source)
        {
            OnUnloaded(source);
        }
        #endregion

        #region 生命周期方法
        /// <summary>
        /// Source加载完毕。
        /// 触发条件：NbPage和NbWindow第一次加载完成时。
        /// </summary>
        /// <param name="source"></param>
        protected virtual void OnLoaded(DependencyObject source)
        {
        }

        /// <summary>
        /// Source卸载完毕。
        /// 触发条件：NbPage卸载移除完成时。
        /// </summary>
        /// <param name="source"></param>
        protected virtual void OnUnloaded(DependencyObject source)
        {
        }
        #endregion
    }
}
