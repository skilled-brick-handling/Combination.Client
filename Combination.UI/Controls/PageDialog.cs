﻿using System;
using System.Windows.Controls;
using System.Windows;

namespace Combination.UI
{
    public static class PageDialog
    {
        private static Window GetMainWindow()
        {
            Window win = null;
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    win = GetMainWindow();
                }));
                return win;
            }
            return Application.Current.MainWindow;
        }

        public static MessageButtons Show(Window owner, string title, Page content, MessageButtons buttons)
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                var result = MessageButtons.Cancel;
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    result = Show(owner, title, content, buttons);
                }));
                return result;
            }
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                Window win = new();
                win.Style = Application.Current.FindResource("NUI.Window.FormStyle") as Style;
                win.WindowStartupLocation = owner == null ? WindowStartupLocation.CenterScreen : WindowStartupLocation.CenterOwner;
                win.ResizeMode = ResizeMode.CanMinimize;
                win.Height = content.Height + NUI.GetAttachedValue<CaptionHeightProperty,double>(win) + 45;
                win.Width = content.Width;
                win.Title = title;
                win.Owner = owner;
                NUI.SetAttachedValue<ShowMinButtonProperty, bool>(win, true);
                NUI.SetAttachedValue<ShowMaxButtonProperty, bool>(win, false);
                NUI.SetAttachedValue<ShowCloseButtonProperty, bool>(win, true);
                NUI.SetAttachedValue<ShowIconProperty, bool>(win, false);
                NUI.SetAttachedValue<PagesProperty, Page>(win, content);
                NUI.SetAttachedValue<MessageButtonsProperty, MessageButtons>(win, buttons);
                win.ShowDialog();
            }));
            return buttons;
        }
        public static MessageButtons Show(string title, Page content, MessageButtons buttons)
        {
            return Show(GetMainWindow(), title, content, buttons);
        }

        public static MessageButtons Show(Page content, MessageButtons buttons)
        {
            return Show(GetMainWindow(), string.Empty, content, buttons);
        }
    }

    public enum MessageButtons
    {
        Ok = 2,
        Cancel = 4,
        Close = 8,
    }
}
