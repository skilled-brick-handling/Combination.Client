﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Combination.UI
{
    /// <summary>
    /// 图标（支持svg和图片路径）。
    /// </summary>
    public class CbIcon : Control
    {
        #region ctor

        static CbIcon()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CbIcon), new FrameworkPropertyMetadata(typeof(CbIcon)));
        }

        #endregion ctor

        #region prop dp

        /// <summary>
        /// 图标内容
        /// </summary>
        public object Data
        {
            get { return (object)GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Data.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DataProperty =
            DependencyProperty.Register("Data", typeof(object), typeof(CbIcon), new FrameworkPropertyMetadata(null, (d, e) =>
            {
                var type = e.NewValue?.GetType();
                var ret = false;
                if (type == typeof(string))
                {
                    try
                    {
                        d.SetValue(DataTypeProperty, CbIconDataType.Geometry);
                        d.SetValue(DataProperty, PathGeometry.Parse(e.NewValue.ToString()));
                        ret = true;
                    }
                    catch { }
                    if (!ret)
                    {
                        try
                        {
                            d.SetValue(DataTypeProperty, CbIconDataType.ImageSource);
                            d.SetValue(DataProperty, new BitmapImage(new Uri(e.NewValue.ToString(), UriKind.RelativeOrAbsolute)));
                            ret = true;
                        }
                        catch { }
                    }
                }
                else if (typeof(Geometry).IsAssignableFrom(type))
                {
                    try
                    {
                        d.SetValue(DataTypeProperty, CbIconDataType.Geometry);
                        d.SetValue(DataProperty, e.NewValue);
                    }
                    catch { }
                }
                else if (typeof(ImageSource).IsAssignableFrom(type))
                {
                    try
                    {
                        d.SetValue(DataTypeProperty, CbIconDataType.ImageSource);
                        d.SetValue(DataProperty, e.NewValue);
                    }
                    catch { }
                }
            }));

        /// <summary>
        /// 图标类型
        /// </summary>
        public CbIconDataType DataType
        {
            get { return (CbIconDataType)GetValue(DataTypeProperty); }
            set { SetValue(DataTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DataType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DataTypeProperty =
            DependencyProperty.Register("DataType", typeof(CbIconDataType), typeof(CbIcon), new PropertyMetadata(CbIconDataType.Geometry));

        /// <summary>
        /// 圆角
        /// </summary>
        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CornerRadius.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(CbIcon), new PropertyMetadata(new CornerRadius()));

        #endregion prop dp

    }

    /// <summary>
    /// CbIcon图标格式
    /// </summary>
    public enum CbIconDataType
    {
        /// <summary>
        /// svg
        /// </summary>
        Geometry = 0,

        /// <summary>
        /// 图片
        /// </summary>
        ImageSource = 1,
    }
}
