﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

namespace Combination.UI
{
    public partial class NUI
    {
        #region Some values 值

        public static bool True { get; } = true;

        public static bool False { get; } = false;

        #endregion

        #region IsInDesignMode 设计器模式状态
        public static bool IsInDesignMode
        {
            get
            {
                return (bool)DesignerProperties.IsInDesignModeProperty.GetMetadata(typeof(DependencyObject)).DefaultValue;
            }
        }
        #endregion

        #region 附加属性值
        public static void SetAttachedValue<Parent, Property>(DependencyObject dp, Property value) where Parent : new()
        {
            BaseAttachedProperty<Parent, Property>.SetValue(dp, value);
        }

        public static Property GetAttachedValue<Parent, Property>(DependencyObject dp) where Parent : new()
        {
            return BaseAttachedProperty<Parent, Property>.GetValue(dp);
        }
        #endregion

        #region 验证输入框
        /// <summary>
        /// 判断一个字符串是否是整数
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static bool IsInt(string val, long minVal, long maxVal)
        {
            if (string.IsNullOrWhiteSpace(val))
                return false;

            if (long.TryParse(val, out long intVal) && intVal < minVal || intVal > maxVal)
            {
                return false;
            }

            return Regex.IsMatch(val, "(^([0-9/-])([0-9]*)$)");
        }

        /// <summary>
        /// 判断一个字符串是否是数字
        /// </summary>
        /// <param name="val"></param>
        /// <param name="decimals">最大小数位数</param>
        /// <returns></returns>
        public static bool IsNumber(string val, int decimals, double minVal, double maxVal)
        {
            if (string.IsNullOrWhiteSpace(val))
                return false;
            if (decimals <= 0 && val.Contains('.'))
                return false;

            if (double.TryParse(val, out double dVal) && dVal < minVal || dVal > maxVal)
            {
                return false;
            }

            var ret = Regex.IsMatch(val, "(^([0-9/-])([0-9]*[.]{0,1}[0-9]*)([0-9])$)|(^[0-9/-]$)|(^([0-9/-])([0-9]+)[.]$)|(^([0-9]+)[.]$)");
            if (ret && val.Contains(".") && val.Split('.')[1].Length > decimals)
            {
                return false;
            }
            return ret;
        }

        /// <summary>
        /// 正则匹配
        /// </summary>
        /// <param name="val"></param>
        /// <param name="regex"></param>
        /// <returns></returns>
        public static bool IsMatch(string val, string regex)
        {
            return Regex.IsMatch(val, regex);
        }

        #endregion

        #region UniformPanel Methods
        public static bool AreClose(double value1, double value2) =>
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            value1 == value2 || IsVerySmall(value1 - value2);
        public static bool IsVerySmall(double value) => Math.Abs(value) < 1E-06;
        public static bool GreaterThan(double value1, double value2) => value1 > value2 && !AreClose(value1, value2);

        #endregion
    }
}
