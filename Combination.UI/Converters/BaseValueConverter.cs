﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace Combination.UI
{
    /// <summary>
    /// 转换类型基类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseValueConverter<T> : MarkupExtension, IValueConverter, IMultiValueConverter
        where T : class, new()
    {
        #region public Members Variable

        /// <summary>
        ///  A static value member
        /// </summary>
        public static T mConverter = null;

        #endregion public Members Variable

        /// <summary>
        ///  Return Static Value
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return mConverter ?? (mConverter = new T());
        }

        #region public value methods

        /// <summary>
        ///  抽象转换方法
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public virtual object Convert(object value, Type targetType, object parameter, CultureInfo culture) => null;

        /// <summary>
        /// 返回转换值
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public virtual object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => null;

        /// <summary>
        ///  多值抽象转换方法
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public virtual object Convert(object[] values, Type targetType, object parameter, CultureInfo culture) => null;

        /// <summary>
        /// 返回转换值
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public virtual object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture) => null;

        #endregion public value methods
    }
}
