﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Combination.UI
{
    public class EnumShowConverter : BaseValueConverter<EnumShowConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && parameter != null && value is Enum && parameter is Enum)
            {
                if (value.Equals(parameter))
                {
                    return Visibility.Visible;
                }
                if (((Enum)value).HasFlag((Enum)parameter))
                {
                    return Visibility.Visible;
                }
            }
            return Visibility.Collapsed;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
