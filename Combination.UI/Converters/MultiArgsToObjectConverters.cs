﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Combination.UI
{
    public class MultiArgsToObjectConverters : BaseValueConverter<MultiArgsToObjectConverters>
    {
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture) => values.Clone();

        public override object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)=> new object[] { DependencyProperty.UnsetValue, DependencyProperty.UnsetValue };
    }
}
