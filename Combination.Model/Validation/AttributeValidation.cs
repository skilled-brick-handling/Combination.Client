using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Combination.Model
{
    /// <summary>
    /// 属性有效性验证
    /// </summary>
    public class AttributeValidation
    {
        /// <summary>
        /// 验证属性
        /// </summary>
        /// <param name="model"></param>
        /// <returns>未通过验证的结果</returns>
        public static List<ResponseValidationResult> Validate(object value)
        {
            var result = new List<ResponseValidationResult>();
            try
            {
                var validationContext = new ValidationContext(value, null, null);
                var results = new List<ValidationResult>();
                var checkAccess = Validator.TryValidateObject(value, validationContext, results, true);
                if (!checkAccess)
                {
                    foreach (var item in results)
                    {
                        result.Add(ResponseValidationResult.Error(item.ErrorMessage));
                    }
                }
            }
            catch (Exception ex)
            {
                result.Add(ResponseValidationResult.Error(ex.Message));
            }
            return result;
        }
    }
}