﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Combination.Model
{
    public class ResponseValidationResult
    {
        /// <summary>
        /// 验证是否通过
        /// </summary>
        public bool CheckAccess { get; set; }

        /// <summary>
        /// 验证失败的消息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 错误
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static ResponseValidationResult Error(string message)
        {
            return new ResponseValidationResult() { CheckAccess = false, Message = message };
        }

        /// <summary>
        /// 成功
        /// </summary>
        /// <returns></returns>
        public static ResponseValidationResult Ok()
        {
            return new ResponseValidationResult() { CheckAccess = true, Message = string.Empty };
        }
    }
}
