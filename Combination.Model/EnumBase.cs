﻿using System.ComponentModel;

namespace Combination.Model
{
    public enum enumConfigValueType
    {
        String = 0,
        Int32 = 1,
        Double = 2,
        Enum = 3,
        FilePath = 4,
        FileBase64 = 5,
        FolderPath = 6,
        Page = 7,
        Time = 8,
        DateTime = 9
    }

    /// <summary>
    /// 操作员账号状态
    /// </summary>
    public enum enumUserStatus
    {
        /// <summary>
        /// 正常
        /// </summary>
        [Description("正常")]
        Normal = 0,
        /// <summary>
        /// 停用
        /// </summary>
        [Description("停用")]
        Stop = 1,
        /// <summary>
        /// 锁定
        /// </summary>
        [Description("锁定")]
        Lock = 2,
    }

}
