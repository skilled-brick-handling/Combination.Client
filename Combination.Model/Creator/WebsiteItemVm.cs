﻿
namespace Combination.Model
{
    public class WebsiteItemVm : BaseVm
    {
        private WebsiteDataVm _websiteData;

        public WebsiteDataVm WebsiteData
        {
            get { return _websiteData; }
            set { _websiteData = value; OnPropertyChanged(); }
        }
    }
}
