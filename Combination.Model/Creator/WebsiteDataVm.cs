﻿namespace Combination.Model
{
    public class WebsiteDataVm : BaseVm
    {
        private string _id;

        public string ID
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged(); }
        }

        protected string _name;
        public virtual string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged(); }
        }

        protected bool _isSelected;
        public virtual bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; OnPropertyChanged(); }
        }

        protected object _image;
        public virtual object Image
        {
            get { return _image; }
            set { _image = value; OnPropertyChanged(); }
        }

        protected string _url;
        public virtual string Url
        {
            get { return _url; }
            set { _url = value; OnPropertyChanged(); }
        }

        protected bool _isPop;
        public virtual bool IsPop
        {
            get { return _isPop; }
            set { _isPop = value; OnPropertyChanged(); }
        }
    }
}
