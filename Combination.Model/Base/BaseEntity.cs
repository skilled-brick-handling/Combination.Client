using System.ComponentModel;

namespace Combination.Model
{
    [Serializable]
    public class BaseEntity : BaseVm, IEntity, INotifyPropertyChanged
    {
        /// <summary>
        /// 无效ID
        /// </summary>
        public static readonly int InvalidID = -1;

        private int _id;
        /// <summary>
        /// ID
        /// </summary>
        public int ID
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
                OnPropertyChanged();
            }
        }
    }
}
