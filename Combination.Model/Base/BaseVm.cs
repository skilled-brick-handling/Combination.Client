using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Combination.Model
{
    [Serializable]
    public class BaseVm : INotifyPropertyChanged
    {
        /// <summary>
        /// 属性值改变事件
        /// </summary>   
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// 触发属性值改变事件
        /// </summary>
        /// <param name="propertyName"></param>
        public virtual void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
