
namespace Combination.Model
{
    public interface IEntity
    {
        int ID { get; set; }
    }
}
