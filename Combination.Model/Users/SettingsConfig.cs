namespace Combination.Model
{
    public class SettingsConfig : BaseEntity, ISort
    {
        #region Protected members
        protected int _groupSortNo, _sortNo;

        protected string _groupName, _name, _key, _value, _ex1, 
            _ex2, _ex3, _data, _cultrueKey, _remark;

        protected enumConfigValueType _valueType;

        protected bool _isReadOnly;
        #endregion

        #region Public Binding Properties

        public virtual int GroupSortNo
        {
            get { return _groupSortNo; }
            set { _groupSortNo = value; OnPropertyChanged(); }
        }
        public virtual int SortNo
        {
            get { return _sortNo; }
            set { _sortNo = value; OnPropertyChanged(); }
        }
        public virtual string GroupName
        {
            get { return _groupName; }
            set { _groupName = value; OnPropertyChanged(); }
        }
        public virtual string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged(); }
        }
        public virtual string Key
        {
            get { return _key; }
            set { _key = value; OnPropertyChanged(); }
        }
        public virtual string Value
        {
            get { return _value; }
            set { _value = value; OnPropertyChanged(); }
        }
        public virtual enumConfigValueType ValueType
        {
            get { return _valueType; }
            set { _valueType = value; OnPropertyChanged(); }
        }
        public virtual string Ex1
        {
            get { return _ex1; }
            set { _ex1 = value; OnPropertyChanged(); }
        }
        public virtual string Ex2
        {
            get { return _ex2; }
            set { _ex2 = value; OnPropertyChanged(); }
        }
        public virtual string Ex3
        {
            get { return _ex3; }
            set { _ex3 = value; OnPropertyChanged(); }
        }
        public virtual string Data
        {
            get { return _data; }
            set { _data = value; OnPropertyChanged(); }
        }
        public virtual bool IsReadOnly
        {
            get { return _isReadOnly; }
            set { _isReadOnly = value; OnPropertyChanged(); }
        }
        public virtual string CultrueKey
        {
            get { return _cultrueKey; }
            set { _cultrueKey = value; OnPropertyChanged(); }
        }
        public virtual string Remark
        {
            get { return _remark; }
            set { _remark = value; OnPropertyChanged(); }
        }
        #endregion
    }

    public class EnumConfigItem : BaseVm
    {
        #region Protected members
        protected string _enumName, _enumValue;
        #endregion

        #region Public Binding Properties
        public virtual string EnumName
        {
            get { return _enumName; }
            set { _enumName = value; OnPropertyChanged(); }
        }
        public virtual string EnumValue
        {
            get { return _enumValue; }
            set { _enumValue = value; OnPropertyChanged(); }
        }
        #endregion
    }
}
