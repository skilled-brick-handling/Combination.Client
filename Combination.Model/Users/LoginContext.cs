﻿using Combination.Model;
using System;
using System.Collections.Generic;

namespace Combination.Model
{
    /// <summary>
    /// 系统登录后，加载的数据。
    /// </summary>
    public class LoginContext
    {
        /// <summary>
        /// 登录时间
        /// </summary>
        public DateTime LoginTime { get; set; }
        /// <summary>
        /// 操作员信息
        /// </summary>
        public UserInfo User { get; } = new UserInfo();

        /// <summary>
        /// 角色信息
        /// </summary>
        public RoleInfo Role { get; } = new RoleInfo();

        /// <summary>
        /// 角色的权限信息
        /// </summary>
        public List<RolePermissionView> RolePermissions { get; } = new List<RolePermissionView>();

    }
}
