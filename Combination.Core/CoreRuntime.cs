﻿using Combination.Data.Service;
using Combination.Model;

namespace Combination.Core
{
    public class CoreRuntime
    {
        #region static properties

        /// <summary>
        /// 数据库相关操作
        /// </summary>
        //public static DbService DbService => DbService.Default;

        /// <summary>
        /// 登录信息
        /// </summary>
        public static LoginContext Context { get; } = new LoginContext();

        #endregion
    }
}
