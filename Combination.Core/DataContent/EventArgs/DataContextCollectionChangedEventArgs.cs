﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Combination.Core
{
    public class DataContextCollectionChangedEventArgs : EventArgs
    {
        public Type DataType { get; }

        public string Name { get; }

        public IEnumerable Items { get; }

        public DataContextCollectionChangedAction Action { get; }


        public DataContextCollectionChangedEventArgs(Type dataType, string name, IEnumerable items, DataContextCollectionChangedAction action)
        {
            DataType = dataType;
            Name = name;
            Items = items;
            Action = action;
        }
    }
}
