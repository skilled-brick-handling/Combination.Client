﻿
namespace Combination.Core
{
    public class WebsiteData : IDataCollection
    {
        public string ID { get; set; }

        public string Name { get; set; }

        public bool IsSelected { get; set; }

        public string Image { get; set; }

        public string Url { get; set; }

        public bool IsPop { get; set; }
    }
}
