using System;
using System.Windows.Media;

namespace Combination.Core
{
    public interface IHelper
    {
        T CopyFrom<T>(object source, bool ignoreNull = false)
            where T : new();
        string GetBase64Image(string fileName, int maxWidth, int maxHeight);
        ImageSource GetImageFromBase64(string base64);
    }
}
