﻿using System;
using System.IO;

namespace Combination.Core
{
    public static class GlobalConst
    {
        /// <summary>
        /// 开发者登录账号
        /// </summary>
        public static readonly string SuperDevLoginName = "superdev";

        /// <summary>
        /// 登录记录保存路径
        /// </summary>
        public static readonly string LoginRecordSavePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "login.json");
    }
}
