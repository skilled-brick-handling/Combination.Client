﻿using Combination.UI;
using System.Windows.Controls;

namespace Combination.Client
{
    /// <summary>
    /// BulidPage.xaml 的交互逻辑
    /// </summary>
    public partial class BuliderPage : BasePage<BuliderPageVm>
    {
        public BuliderPage()
        {
            InitializeComponent();
        }

        public BuliderPage(BuliderPageVm specificViewModel) : base(specificViewModel)
        {
            InitializeComponent();
        }
    }
}
