﻿using Combination.UI;
using Microsoft.Extensions.DependencyInjection;
using System.ComponentModel;

namespace Combination.Client
{
    public class BasePage<VM> : BasePage
        where VM : PageVm, new()
    {
        #region Public Properties

        public VM ViewModel
        {
            get => (VM)ViewModelObject;
            set => ViewModelObject = value;
        }

        #endregion

        public BasePage()
        {
            if (DesignerProperties.GetIsInDesignMode(this))
                ViewModel = new VM();
            else
                ViewModel = App.Host.Services.GetService<VM>() ?? new VM();
        }

        public BasePage(VM specificViewModel = null)
        {
            if (specificViewModel != null)
                ViewModel = specificViewModel;
            else
            {
                if (DesignerProperties.GetIsInDesignMode(this))
                    ViewModel = new VM();
                else
                    ViewModel = App.Host.Services.GetService<VM>() ?? new VM();
            }
        }

    }
}
