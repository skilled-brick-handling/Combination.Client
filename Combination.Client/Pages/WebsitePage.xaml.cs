﻿using Combination.UI;
using System.Windows.Controls;

namespace Combination.Client
{
    /// <summary>
    /// WebsitePage.xaml 的交互逻辑
    /// </summary>
    public partial class WebsitePage : BasePage<WebsitePageVm>
    {
        public WebsitePage()
        {
            InitializeComponent();
        }

        public WebsitePage(WebsitePageVm specificViewModel) : base(specificViewModel)
        {
            InitializeComponent();
        }
    }
}
