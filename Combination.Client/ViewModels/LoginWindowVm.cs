﻿using Combination.Comm;
using Combination.Core;
using Combination.Model;
using Combination.UI;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Combination.Client
{
    public class LoginWindowVm : UIVm
    {
        #region private Members
        private LoginWindow _loginWindow;
        #endregion

        #region public Commands
        public ICommand LoginCommand { get; }

        #endregion

        #region properties
        protected string _loginName;
        /// <summary>
        /// 登录账号
        /// </summary>
        public virtual string LoginName
        {
            get { return _loginName; }
            set
            {
                _loginName = value;
                OnPropertyChanged(nameof(LoginName));
                if (!string.IsNullOrWhiteSpace(value))
                {
                    var rec = LoginRecordList?.FirstOrDefault(p => p.LoginName?.Equals(value, StringComparison.OrdinalIgnoreCase) == true);
                    if (rec != null)
                    {
                        Pwd = rec.Pwd;
                        RememberPwd = rec.Remember;
                    }
                    else
                    {
                        Pwd = string.Empty;
                        RememberPwd = false;
                    }
                }
            }
        }

        protected string _pwd;

        /// <summary>
        /// 密码
        /// </summary>
        public virtual string Pwd
        {
            get { return _pwd; }
            set { _pwd = value; OnPropertyChanged(nameof(Pwd)); }
        }

        protected bool _rememberPwd;

        public virtual bool RememberPwd
        {
            get { return _rememberPwd; }
            set { _rememberPwd = value; OnPropertyChanged(nameof(RememberPwd)); }
        }

        protected ObservableCollection<LoginRecord> _loginRecordList;

        public virtual ObservableCollection<LoginRecord> LoginRecordList
        {
            get { return _loginRecordList; }
            set { _loginRecordList = value; OnPropertyChanged(nameof(LoginRecordList)); }
        }


        protected LoginRecord _selectedLoginRecord;

        public virtual LoginRecord SelectedLoginRecord
        {
            get { return _selectedLoginRecord; }
            set
            {
                _selectedLoginRecord = value;
                OnPropertyChanged(nameof(SelectedLoginRecord));
            }
        }
        #endregion

        #region ctor
        public LoginWindowVm()
        {
            LoginCommand = new UICommand(Login);
            LoadItems();
        }
        #endregion

        #region Public Methods

        public void LoadItems()
        {
            try
            {
                var loginRecs = JsonHelper.FromJsonFile<List<LoginRecord>>(GlobalConst.LoginRecordSavePath) ?? new List<LoginRecord>();
                loginRecs.ForEach(p => p.Pwd = PasswordHelper.Decrypt(p.Pwd));
                this.LoginRecordList = new ObservableCollection<LoginRecord>(loginRecs);
                this.SelectedLoginRecord = LoginRecordList.FirstOrDefault();
                if (this.SelectedLoginRecord != null && this.SelectedLoginRecord.Remember)
                {
                    this.Pwd = this.SelectedLoginRecord.Pwd;
                }
            }
            catch
            {
                this.LoginRecordList = new ObservableCollection<LoginRecord>();
            }
        }

        public void Login(object obj)
        {
            _loginWindow.DialogResult = true;
            _loginWindow.Close();
        }

        public void OnViewInitialized(LoginWindow loginWindow)
        {
            _loginWindow = loginWindow;
        }
        #endregion
    }
}
