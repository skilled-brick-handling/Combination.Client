﻿using Combination.Comm;
using Combination.Model;
using Combination.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Combination.Client
{
    public class SettingWindowVm : UIVm
    {
        #region public Commands
        public ICommand CloseCommand { get; }

        public ICommand SaveCommand { get; set; }

        #endregion

        public Page CurrentPage { get; } = new WebsitePage();

        #region ctor
        public SettingWindowVm()
        {
            CloseCommand = new UICommand(Close);
            SaveCommand = new UICommand(Save);
        }

        #endregion

        #region Public Methods

        private SettingWindow _settingsWindow;

        public void OnViewInitialized(SettingWindow settingsWindow)
        {
            this._settingsWindow = settingsWindow;
        }
        #endregion

        #region Private Methods

        private void Close(object obj)
        {
            this._settingsWindow?.Close();
        }

        private void Save(object obj)
        {

        }

        #endregion
    }

    public class SettingsConfigItem : SettingsConfig
    {
        public SettingsConfigItem(SettingsConfig config)
        {
            config.PropertyCopyTo(this);
            if (config.ValueType == enumConfigValueType.String && int.TryParse(config.Ex1, out int maxLength))
            {
                MaxLength = maxLength;
            }
            else if ((config.ValueType == enumConfigValueType.Int32 || config.ValueType == enumConfigValueType.Double)
                && double.TryParse(config.Ex1, out double minValue) && double.TryParse(config.Ex2, out double maxValue))
            {
                MinValue = minValue;
                MaxValue = maxValue;
            }
            else if (config.ValueType == enumConfigValueType.Enum && !string.IsNullOrWhiteSpace(config.Ex1) && config.Ex1.TryToJsonObject(out List<EnumConfigItem> items))
            {
                items.ForEach(p => p.EnumName = p.EnumName);//语言
                EnumConfigItems = new ObservableCollection<EnumConfigItem>(items);
                SelectedEnumConfigItem = EnumConfigItems.FirstOrDefault(p => p.EnumValue == config.Value);
            }
        }

        #region Protected Members

        protected double _minValue , _maxValue;

        protected int _maxLength;

        protected EnumConfigItem _selectedEnumConfigItem;

        protected ObservableCollection<EnumConfigItem> _enumConfigItems;

        #endregion

        #region Public Properties
        public virtual double MinValue
        {
            get { return _minValue; }
            set { _minValue = value; OnPropertyChanged(); }
        }
        public virtual double MaxValue
        {
            get { return _maxValue; }
            set { _maxValue = value; OnPropertyChanged(); }
        }
        public virtual int MaxLength
        {
            get { return _maxLength; }
            set { _maxLength = value; OnPropertyChanged(); }
        }        

        public virtual EnumConfigItem SelectedEnumConfigItem
        {
            get { return _selectedEnumConfigItem; }
            set
            {
                _selectedEnumConfigItem = value;
                OnPropertyChanged();
                Value = SelectedEnumConfigItem?.EnumValue ?? string.Empty;
            }
        }

        public virtual ObservableCollection<EnumConfigItem> EnumConfigItems
        {
            get { return _enumConfigItems; }
            set { _enumConfigItems = value; OnPropertyChanged(); }
        }

        public virtual TimeSpan TimeValue
        {
            get { TimeSpan.TryParse(base.Value, out TimeSpan val); return val; }
            set
            {
                base.Value = value.ToString();
                OnPropertyChanged();
            }
        }
        #endregion
    }
    public class SystemConfigItemTemplateSelector : DataTemplateSelector
    {
        /// <summary>
        /// 字符串配置模板
        /// </summary>
        public DataTemplate TextItem { get; set; }
        /// <summary>
        /// 整数配置模板
        /// </summary>
        public DataTemplate IntItem { get; set; }
        /// <summary>
        /// 小数配置模板
        /// </summary>
        public DataTemplate DoubleItem { get; set; }
        /// <summary>
        /// 枚举列表配置模板
        /// </summary>
        public DataTemplate EnumItem { get; set; }
        /// <summary>
        /// 文件/目录路径配置模板
        /// </summary>
        public DataTemplate PathItem { get; set; }
        /// <summary>
        /// page配置模板
        /// </summary>
        public DataTemplate PageItem { get; set; }

        /// <summary>
        /// 时间配置模板
        /// </summary>
        public DataTemplate TimeItem { get; set; }

        /// <summary>
        /// 日期时间配置模板
        /// </summary>
        public DataTemplate DateTimeItem { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="container"></param>
        /// <returns></returns>
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var configItem = item as SettingsConfigItem;
            if (configItem != null)
            {
                if (configItem.ValueType == enumConfigValueType.String)
                    return TextItem;
                if (configItem.ValueType == enumConfigValueType.Int32)
                    return IntItem;
                if (configItem.ValueType == enumConfigValueType.Double)
                    return DoubleItem;
                if (configItem.ValueType == enumConfigValueType.Enum)
                    return EnumItem;
                if (configItem.ValueType == enumConfigValueType.FilePath ||
                    configItem.ValueType == enumConfigValueType.FileBase64 ||
                    configItem.ValueType == enumConfigValueType.FolderPath)
                    return PathItem;
                if (configItem.ValueType == enumConfigValueType.Page)
                    return PageItem;
                if (configItem.ValueType == enumConfigValueType.Time)
                    return TimeItem;
                if (configItem.ValueType == enumConfigValueType.DateTime)
                    return DateTimeItem;
            }

            return TextItem;
        }
    }
}
