﻿using Combination.Model;
using Combination.UI;
using System.Windows;
using System.Windows.Input;

namespace Combination.Client
{
    public class WebsitePageVm : PageVm
    {
		private string _text ="Hello,World";

		public string Text
		{
			get { return _text; }
			set { _text = value; }
		}
        #region public Commands
        public ICommand AddCommand { get; }

        public ICommand SyncCommand { get; }
        public ICommand ManageCommand { get; }
        
        #endregion

        #region ctor
        public WebsitePageVm()
        {
            AddCommand = new UICommand(Add);
            SyncCommand = new UICommand(Sync);
            ManageCommand = new UICommand(Manage);
        }
        #endregion

        #region Command Methods
        public static void Add(object obj)
        {
            FormPage page = new AccountEditPage
            {
                DataContext = new AccountEditPageVm(),
                AfterFormValidateHandle = dataContext =>
                {
                    return ResponseValidationResult.Ok();
                }
            };
            PageDialog.Show("添加帐号", page, MessageButtons.Close);
        }

        public static void Sync(object obj)
        {

        }

        public static void Manage(object obj)
        {

        }
        #endregion
    }
}
