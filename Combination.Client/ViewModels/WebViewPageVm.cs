﻿using Combination.Model;
using Combination.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Combination.Client
{
    public class WebViewPageVm : PageVm
    {
        #region public binding

        protected string _url;

        public string Url
        {
            get { return _url; }
            set { _url = value; OnPropertyChanged(); }
        }
        protected string _webViewSource;

        public string WebViewSource
        {
            get { return _webViewSource; }
            set { _webViewSource = value; OnPropertyChanged(); }
        }

        #endregion
        public WebsiteItemVm Website { get; set; }
        public WebViewPageVm(WebsiteItemVm website)
        {
            this.Website = website;
        }
    }
}
