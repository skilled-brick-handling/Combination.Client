﻿using Combination.Core;
using Microsoft.Extensions.DependencyInjection;

namespace Combination.Client
{
    public static class FrameworkConstructionExtensions
    {
        public static IServiceCollection AddCombinationViewModels(this IServiceCollection construction)
        {
            construction.AddSingleton<MainWindowVm>();
            return construction;
        }

        public static IServiceCollection AddCombinationServices(this IServiceCollection construction)
        {
            construction.AddTransient<IHelper, Helper>();

            return construction;
        }
    }
}
