﻿using Combination.UI;
using System;
using System.Globalization;
using System.Windows.Data;

namespace Combination.Client
{
    public class Base64ToImageConverter : BaseValueConverter<Base64ToImageConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            try
            {
                return App.Helper.GetImageFromBase64(value.ToString());
            }
            catch
            {
                return null;
            }
        }
        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
