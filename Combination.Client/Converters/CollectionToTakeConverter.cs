﻿using Combination.Model;
using Combination.UI;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace Combination.Client
{
    public class CollectionToTakeConverter : BaseValueConverter<CollectionToTakeConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }
            var list = (value as ObservableCollection<WebsiteItemVm>).Where(p => p.WebsiteData.IsPop);
            return list;
        }
        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
