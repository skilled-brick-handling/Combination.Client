﻿using System.Windows;

namespace Combination.Client
{
    /// <summary>
    /// SettingWindow.xaml 的交互逻辑
    /// </summary>
    public partial class SettingWindow : Window
    {
        public SettingWindow()
        {
            InitializeComponent();
            settingVm.OnViewInitialized(this);
        }
    }
}
