﻿using Combination.Core;
using Combination.Data.Service;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Configuration;
using System.IO;

namespace Combination.Client.Core
{
    /// <summary>
    /// 一个常用的主机
    /// </summary>
    public class BasicHost : IHost
    {
        private IServiceProvider _services;

        private IConfiguration _config;

        private IAppEnvironment _env;

        public IServiceProvider Services => _services;

        public IConfiguration Config => _config;

        public IAppEnvironment Env => _env;

        public ILoggerProvider LoggerProvider => _services.GetRequiredService<ILoggerProvider>();

        public IDbService DbServices => _services.GetRequiredService<IDbService>();

        public void Init()
        {
            _env = new AppEnvironment();

            _config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true)
                .AddJsonFile($"appsettings.{_env.EnvironmentName}.json", true)
                .Build();

            _services = new ServiceCollection()
                .AddLogging(loggingBuilder =>
                {
                    loggingBuilder.AddDebug();
                })
                .AddSingleton<IDataContext>(new LocalJsonDataContext()
                {
                    ConnectionString = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data")
                })
                .AddSingleton<IDbService>(new DbService()
                {
                     ConnectionString = _config.GetConnectionString("DbConnStr")
                })
                .AddCombinationViewModels()
                .AddCombinationServices()
                .BuildServiceProvider();
        }
    }
}
