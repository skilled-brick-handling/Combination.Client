﻿using Combination.Model;
using System.Linq.Expressions;

namespace Combination.Data.Service
{
    public interface IDbService
    {
        string ConnectionString { get; set; }
        Db CreateDb();

        List<T> GetAll<T>();

        List<T> GetPage<T>(Expression<Func<T, bool>> where,
            int pageNo,
            int pageSize,
            out int total) where T : IEntity;
        List<T> FindAll<T>(Expression<Func<T, bool>> where);

        QueryDbResult<T> Query<T>(Expression<Func<T, bool>> where);

        QueryFirstDbResult<T> QueryFirstOrDefault<T>(Expression<Func<T, bool>> where);

        ExecuteDbResult Insert<T>(T model);

        ExecuteDbResult BatchInsert<T>(List<T> models);

        ExecuteDbResult Update<T>(T model);

        ExecuteDbResult Update<T>(object model, Expression<Func<T, bool>> where);

        ExecuteDbResult BatchUpdate<T>(List<T> models);

        ExecuteDbResult Delete<T>(T model) where T : IEntity;

        ExecuteDbResult Delete<T>(Expression<Func<T, bool>> where);
    }
}
