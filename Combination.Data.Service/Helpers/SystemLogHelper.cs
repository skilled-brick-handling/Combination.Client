using Combination.Model;

namespace Combination.Data.Service
{
    /// <summary>
    /// 系统日志助手
    /// </summary>
    public static class SystemLogHelper
    {
        /// <summary>
        /// 当前用户
        /// </summary>
        public static UserInfo CurrentUser { get; set; }

        /// <summary>
        /// 日志记录
        /// </summary>
        /// <param name="sysLog"></param>
        public static void Log(SystemLogs sysLog)
        {
            if (sysLog == null)
                return;

            try
            {
                using var db = DbService.Default.CreateDb();
                db.Insert<SystemLogs>().Values(sysLog).Execute();
            }
            catch
            {

            }
        }

        /// <summary>
        /// 日志记录
        /// </summary>
        /// <param name="name"></param>
        /// <param name="log"></param>
        /// <param name="level"></param>
        /// <param name="logType"></param>
        /// <param name="sysType"></param>
        public static void Log(string name, string log, enumSystemLogLevel level, enumSystemLogType logType, enumSystemType sysType)
        {
            var sysLog = new SystemLogs()
            {
                Name = name,
                Log = log,
                Level = level,
                SystemLogType = logType,
                SystemType = sysType,
                LogTime = DateTime.Now,
                LoginName = CurrentUser == null ? string.Empty : $"{CurrentUser.Name}({CurrentUser.LoginName})"
            };
            Log(sysLog);
        }

        /// <summary>
        /// 用户操作日志
        /// </summary>
        /// <param name="name"></param>
        /// <param name="log"></param>
        /// <param name="sysType"></param>
        public static void UserOptLog(string name, string log, enumSystemType sysType)
        {
            Log(name, log, enumSystemLogLevel.Info, enumSystemLogType.UserOpt, sysType);
        }

        /// <summary>
        /// 管理系统用户操作记录
        /// </summary>
        /// <param name="name"></param>
        /// <param name="log"></param>
        /// <param name="sysType"></param>
        public static void MgrUserOptLog(string name, string log)
        {
            Log(name, log, enumSystemLogLevel.Info, enumSystemLogType.UserOpt, enumSystemType.MonitorMgr);
        }
    }
}
